package com.niisokb.test.classes;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.niisokb.test.utils.HttpCommand;

public class Element {

	public int _id;
	public int server_id;
	public String title;
	public String img;
	
	public static boolean clear(SQLiteDatabase db) {
		try {
			db.execSQL("DELETE FROM elements WHERE 1");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static Element getElementById(SQLiteDatabase db,int _id) {
		Cursor cursor = null;
		try {
			cursor = db.rawQuery("SELECT _id, server_id, title, img FROM elements WHERE _id = ?", new String[] {String.valueOf(_id)});
			if (cursor.moveToNext()) {
				Element element = new Element();
				element._id = cursor.getInt(cursor.getColumnIndex("_id"));
				element.server_id = cursor.getInt(cursor.getColumnIndex("server_id"));
				element.title = cursor.getString(cursor.getColumnIndex("title"));
				element.img = cursor.getString(cursor.getColumnIndex("img"));
				return element;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor!=null) cursor.close();
		}
		return null;
	}

	public static ArrayList<Element> getElements(SQLiteDatabase db) {
		ArrayList<Element> elements = new ArrayList<Element>();
		Cursor cursor = null;
		try {
			cursor = db.rawQuery("SELECT _id, server_id, title, img FROM elements WHERE 1",null);
			while (cursor.moveToNext()) {
				Element element = new Element();
				element._id = cursor.getInt(cursor.getColumnIndex("_id"));
				element.server_id = cursor.getInt(cursor.getColumnIndex("server_id"));
				element.title = cursor.getString(cursor.getColumnIndex("title"));
				element.img = cursor.getString(cursor.getColumnIndex("img"));
				elements.add(element);
			}
			return elements;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor!=null) cursor.close();
		}
		return elements;
	}
	
	public long save(SQLiteDatabase db) {
		try {
			ContentValues insertValues = new ContentValues();
			insertValues.put("server_id", server_id);
			insertValues.put("title", title);
			insertValues.put("img", img);
			return db.insert("elements", null, insertValues);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	public static ArrayList<Element> getElementsFromServer(SQLiteDatabase db) {
		ArrayList<Element> elements = new ArrayList<Element>();
		try {
			HttpCommand httpCommand = new HttpCommand("http://jsontest111.apiary.io/androids", true);
			JSONArray array = new JSONArray(httpCommand.executeForString());
			if (array.length()>0) clear(db);
			for (int i=0;i<array.length();i++) {
				JSONObject object = array.getJSONObject(i);
				Element element = new Element();
				element.server_id = object.getInt("id");
				element.title = object.getString("title");
				element.img = object.getString("img");
				element._id = (int) element.save(db);
				elements.add(element);
			}
			return elements;
		} catch(Exception e) {
			e.printStackTrace();
			return elements;
		}
	}
	
}
