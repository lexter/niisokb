package com.niisokb.test.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import android.util.Base64;
import android.util.Log;


public class HttpCommand {
	
	//private String host = MailcourierApplication.ServerURL;
	//private String host = "mailbox.opt-service.com";
	
	private List<BasicNameValuePair> pairs;
	private HttpGet requestGet;
	private HttpPost requestPost;
	private boolean isItGet=false;
	private Header[] headers;
	private StatusLine statusline=null;
	private InputStream stream;
	
	
	public HttpCommand(String url, boolean isItGet) {
		this.isItGet = isItGet;
		
		if (isItGet) {
			requestGet = new HttpGet(url);
			//requestGet.addHeader("Authorization", "Basic " + auth);
		} else {
			requestPost = new HttpPost(url);
			//requestPost.addHeader("Authorization", "Basic " + auth);
		}
		pairs = new ArrayList<BasicNameValuePair>();
	}
	
//	public void setMultipartEntity(MultipartEntity ent) {
//		requestPost.setEntity(ent);
//	}
	
	public void addParam(String Name, String Value) {
		pairs.add(new BasicNameValuePair(Name,Value));
	}
	
	public String executeForString() throws Exception {
				
		String result = "";
		try {
			execute();
			BufferedReader reader = new BufferedReader(new InputStreamReader(stream,"utf-8"));
			StringBuilder sb = new StringBuilder();
	        String line = null;
	        while ((line = reader.readLine()) != null) {
	      	  sb.append(line + System.getProperty("line.separator"));
	        }    
	        result = sb.toString();
		} catch (Exception e) {
			result = "";
			throw e;
		}
		return result;
	}


	public void setContent(String msg) {
		try {
			if (isItGet) {
				//requestGet.setEntity(new StringEntity(msg, HTTP.UTF_8));
			} else {
				requestPost.setEntity(new StringEntity(msg, HTTP.UTF_8));
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public void setContentForJson(String msg) {
		try {
			StringEntity se = new StringEntity(msg);  
	        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			if (isItGet) {
				//requestGet.setEntity(se);
			} else {
				requestPost.setEntity(se);
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	
	public void setContent(byte[] data) {
		try {
			
			if (isItGet) {
				//requestGet.setEntity(new StringEntity(msg, HTTP.UTF_8));
			} else {
				requestPost.setEntity(new ByteArrayEntity(data));
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	public HttpResponse execute() throws ClientProtocolException, IOException {
		HttpClient client = new DefaultHttpClient();
		
		client.getParams().setParameter("http.protocol.content-charset", HTTP.UTF_8);
		for (Object item : pairs) {
			BasicNameValuePair element = (BasicNameValuePair)item;
			if (isItGet)
				requestGet.addHeader(element.getName(),URLEncoder.encode(element.getValue(), "UTF-8"));
			else 
				requestPost.addHeader(element.getName(),URLEncoder.encode(element.getValue(), "UTF-8"));
		}
		HttpResponse response = null;
		if (isItGet)
			response = client.execute(requestGet);
		else
			response = client.execute(requestPost);
		headers = response.getAllHeaders();
		stream = response.getEntity().getContent();
		statusline = response.getStatusLine();
	
		return response;
		
	}

	public String getHeader(String name) {
		for (Header header : headers) {
			if (header.getName().equals(name)) {
				return header.getValue();
			}			
		}
		return null;
	}
	
	public HttpPost getRequestPost() {
		return requestPost;
	}
	
	public StatusLine getStatusline() {
		return statusline;
	}

	public InputStream getInputStream() {
		return stream;
	}

}
