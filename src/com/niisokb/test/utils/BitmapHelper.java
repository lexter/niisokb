package com.niisokb.test.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BitmapHelper {

	static int maxsize = 200;
	
	public static Bitmap getPicture(Context context, String urlPicture) {
		
		try {
			URL url = new URL(urlPicture);
			File f = new File(context.getCacheDir()+"/"+url.getFile());
			
			if (f.exists()) return getBitmapFromPath(context,maxsize,urlPicture);
           
			f.getParentFile().mkdirs();
            return getBitmapFromUrl(context, url, f, urlPicture);

	    } catch (Exception e) {
	    	e.printStackTrace();
	    	return null;
	    }
	}
		
	private static synchronized Bitmap getBitmapFromUrl(final Context context, URL url, File f, final String urlPicture) {
		try {
			URLConnection connection = url.openConnection();
	        connection.connect();
	        int fileLength = connection.getContentLength();
	
	        // download the file
	        InputStream input = new BufferedInputStream(url.openStream());
	        OutputStream output = new FileOutputStream(f);
	
	        byte data[] = new byte[1024];
	        long total = 0;
	        int count;
	        while ((count = input.read(data)) != -1) {
	            total += count;
	            output.write(data, 0, count);
	        }
	
	        output.flush();
	        output.close();
	        input.close();
			
			return getBitmapFromPath(context,maxsize,urlPicture);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private static Bitmap getBitmapFromPath(final Context context, int size, final String urlPicture) {
	    try {
			final URL url = new URL(urlPicture);
			File f = new File(context.getCacheDir()+"/"+url.getFile());
	    	if (!f.exists()) {
	    		return null;
	    	}
	        //Decode image size
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(f),null,o);
	
	        final int REQUIRED_SIZE=size;
	
	        int scale=1;
	        while(o.outWidth/scale/2>=REQUIRED_SIZE && o.outHeight/scale/2>=REQUIRED_SIZE)
	            scale*=2;
	
	        System.out.println("scale : "+scale);
	        
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize=scale;
	        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
	    } catch (Exception e) {
	    	e.printStackTrace();
	    	return null;
	    }
	    
	}
		
	

	
}
