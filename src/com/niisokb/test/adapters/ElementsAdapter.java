package com.niisokb.test.adapters;

import java.util.ArrayList;
import java.util.List;

import com.niisokb.test.R;
import com.niisokb.test.classes.Element;
import com.niisokb.test.utils.BitmapHelper;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ElementsAdapter extends ArrayAdapter<Element> {

	List<Element> elements;
	Handler handler;
	Context context;
	
	public ElementsAdapter(Context context, int resource, ArrayList<Element> elements) {
		super(context, resource, elements);
		this.elements = elements;
		this.context = context;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		view = LayoutInflater.from(getContext()).inflate(R.layout.item_main, null);
		TextView title = (TextView) view.findViewById(R.id.textViewTitle);
		title.setText(elements.get(position).title);
		title.setTextColor(Color.BLACK);
		return view;
	}
	
}
