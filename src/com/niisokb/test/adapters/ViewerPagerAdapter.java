package com.niisokb.test.adapters;

import java.util.ArrayList;

import com.niisokb.test.fragments.FragmentElement;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ViewerPagerAdapter extends FragmentPagerAdapter{

    ArrayList<FragmentElement> fragList; 

    public ViewerPagerAdapter(FragmentManager fm, ArrayList<FragmentElement> fragList) {
        super(fm);
        this.fragList = fragList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragList.get(position);
    }

    @Override
    public int getCount() {
        return fragList.size();
    }

}