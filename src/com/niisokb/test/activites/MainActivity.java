package com.niisokb.test.activites;

import java.util.ArrayList;

import com.niisokb.test.R;
import com.niisokb.test.adapters.ElementsAdapter;
import com.niisokb.test.classes.Element;
import com.niisokb.test.db.DbOpenHelper;
import com.niisokb.test.utils.Connectivity;

import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	ListView listView;
	ProgressDialog progress;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		progress = new ProgressDialog(this);
		
		listView = (ListView) findViewById(R.id.listViewElements);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent intent = new Intent(MainActivity.this,ViewerActivity.class);
				intent.putExtra("position", position);
				startActivity(intent);
			}
			
		});
		
		ArrayList<Element> elements = null;
		elements = Element.getElements(DbOpenHelper.getInstance(getApplicationContext()).getDb());
		final ElementsAdapter adapter = new ElementsAdapter(getApplicationContext(), R.layout.item_main, elements);
		listView.setAdapter(adapter);

	}

	Runnable refresh = new Runnable() {
		
		@Override
		public void run() {
			ArrayList<Element> elements = null;
			elements = Element.getElementsFromServer(DbOpenHelper.getInstance(getApplicationContext()).getDb());
			final ElementsAdapter adapter = new ElementsAdapter(getApplicationContext(), R.layout.item_main, elements);
			listView.post(new Runnable() {
				
				@Override
				public void run() {
					listView.setAdapter(adapter);
					dismissDialog();
				}
			});
			
		}
	};
	
	void showDialog() {
		if (progress!=null) {
			progress.setTitle("Обновление");
	        progress.setMessage("Получение информации...");
	        progress.setIndeterminate(false);
	        progress.setCanceledOnTouchOutside(false);
	        progress.show();
		}
	}
	
	void dismissDialog() {
		if (progress!=null) progress.dismiss();
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_refresh) {
			if (!Connectivity.isConnected(getApplicationContext())) {
				Toast.makeText(getBaseContext(), "Отсутствует подключение.", Toast.LENGTH_SHORT).show();
				return false;
			}
			showDialog();
			new Thread(refresh).start();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
