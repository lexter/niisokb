package com.niisokb.test.activites;

import java.util.ArrayList;

import com.niisokb.test.R;
import com.niisokb.test.adapters.ViewerPagerAdapter;
import com.niisokb.test.classes.Element;
import com.niisokb.test.db.DbOpenHelper;
import com.niisokb.test.fragments.FragmentElement;

import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.os.Bundle;

public class ViewerActivity extends ActionBarActivity {
	
	ViewPager pager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_viewer);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		ArrayList<FragmentElement> fragmentElements = new ArrayList<FragmentElement>();
		ArrayList<Element> elements = Element.getElements(DbOpenHelper.getInstance(getApplicationContext()).getDb());
		for (Element element:elements) {
			fragmentElements.add(new FragmentElement(element));
		}
		
		pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(new ViewerPagerAdapter(getSupportFragmentManager(), fragmentElements));
		pager.setCurrentItem(getIntent().getExtras().getInt("position"));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
