package com.niisokb.test.fragments;

import com.niisokb.test.R;
import com.niisokb.test.classes.Element;
import com.niisokb.test.utils.BitmapHelper;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


public class FragmentElement extends Fragment {

	Handler handler;
	TextView title;
	ImageView imageView;
	ProgressBar bar;
	Element element;
	Thread setPic;
	
	public FragmentElement(Element element) {
		this.element = element;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.page_viewer, container, false);
		setRetainInstance(true);
		title = (TextView) view.findViewById(R.id.textViewTitle);
		title.setText(element.title);
		imageView = (ImageView) view.findViewById(R.id.imageView);
		bar = (ProgressBar) view.findViewById(R.id.progressBar);
		bar.setProgressDrawable(getActivity().getResources().getDrawable(R.drawable.progress_bar));
		
		return view;
	}

	public void onResume() {
		super.onResume();
		if (setPic==null) {
			setPic = new Thread(setPicture);
			setPic.start();
		}
	}
	
	Runnable setPicture = new Runnable() {
		
		@Override
		public void run() {
			final Bitmap bitmap = BitmapHelper.getPicture(getActivity().getApplicationContext(), element.img);
			imageView.post(new Runnable() {
				
				@Override
				public void run() {
					if (bitmap!=null) {
						imageView.setImageBitmap(bitmap);
						bar.setVisibility(View.GONE);
					}
				}
			});
			setPic = null;
		}
	};

}